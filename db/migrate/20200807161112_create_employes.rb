class CreateEmployes < ActiveRecord::Migration[6.0]
  def change
    create_table :employes do |t|
    	t.string :name
    	t.string :address
    	t.string :phone_number
    	t.string :email
    	t.integer :position_id
    	t.integer :company_id

      t.timestamps
    end
  end
end
