class RenameEmailToBranche < ActiveRecord::Migration[6.0]
  def change
  	rename_column :branches, :address, :company_id
  	rename_column :branches, :email, :person_in_charge
  end
end
