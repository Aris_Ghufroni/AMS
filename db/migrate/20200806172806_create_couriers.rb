class CreateCouriers < ActiveRecord::Migration[6.0]
  def change
    create_table :couriers do |t|
    	t.string :name, lenght: 50, default: '-'
    	t.string :address, lenght: 200, default: '-'
    	t.string :phone_number, lenght: 15, default: '-'
    	t.string :email, lenght: 30, default: '-'
    	t.integer :sim, lenght: 30, default: '-'

      t.timestamps
    end
  end
end
