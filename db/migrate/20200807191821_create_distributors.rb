class CreateDistributors < ActiveRecord::Migration[6.0]
  def change
    create_table :distributors do |t|
    	t.string :name
    	t.integer :city_id
    	t.string :address
    	t.string :phone_number
    	t.string :email

      t.timestamps
    end
  end
end
