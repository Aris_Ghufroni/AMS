class RenameNameToBranche < ActiveRecord::Migration[6.0]
  def change
  	rename_column :branches, :name, :address
  end
end
