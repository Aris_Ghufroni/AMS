class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
    	t.string :name, length: 50, default: '-'
    	t.string :address, length: 200, default: '-'
    	t.string :phone_number, length: 15, default: '-'


      t.timestamps
    end
  end
end
