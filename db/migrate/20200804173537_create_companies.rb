class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
    	t.string :name, length: 50, default: '-'
    	t.string :address, lenght: 200, default: '-'
    	t.string :phone_number, length: 15, default: '-'
    	

      t.timestamps
    end
  end
end
