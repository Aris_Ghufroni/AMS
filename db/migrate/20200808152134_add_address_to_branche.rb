class AddAddressToBranche < ActiveRecord::Migration[6.0]
  def change
  	add_column :branches, :city_id, :integer
  	add_column :branches, :address, :integer
  	add_column :branches, :phone_number, :string
  	add_column :branches, :email, :string
  end
end
