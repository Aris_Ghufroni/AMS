Rails.application.routes.draw do

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root :to  => 'customers#index'
  get '/customers/new', :to => 'customers#new', as: 'new'
  get '/companies/new', :to => 'companies#new'
  get '/companies/index', :to => 'companies#index', as: 'index'
  get '/courier/index', :to => 'courier#index', as: 'courier'
  get '/courier/new', :to => 'courier#new', as: 'data'
  get '/distributors/index', :to => 'distributors#index', as: 'agen'
  get '/distributors/new', :to => 'distributors#new', as: 'stok'
  get '/branches/index', :to => 'branches#index', as: 'citie'
  get '/branches/new', :to => 'branches#new', as: 'branc'

  get '/email', :to => 'customers#email'
 #company a
   post 'customer', to: 'customers#create', as: 'customers'
  post 'companie', to: 'companies#create', as: 'companies'
  post 'courier', to: 'courier#create', as: 'couriers'
  post 'distributor', to: 'distributors#create', as: 'distributors'
  post 'branches', to: 'branches#create', as: 'branches'
end



